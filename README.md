# ZemogaPosts
This is an app to show some posts with additional information.

# How to run the app
The repo is ready to be cloned, compiled and tested. No additional steps need to be taken.

# Architecture
- This app is based on a Reactive MVVM architecture, using LiveData and ViewModel components. For data persistence, Room component was used, which together with LiveData and ViewModel, automatic updates to DB can be immediatly reflected on the UI.
- Retrofit was used to communicate with remote API service.
- Repository Pattern was also implemented in order to encapsulate data sources.
- The main advantage of this approach over others is the reactive nature of data changes and the clear separation of concerns.
- The project is divided into 2 modules: Commons and App.
- The commons module houses all logic that might be used by app module, as well as potential feature-modules. It is a very good practice to always create a commons module since it allows a more healthy grow of the project if more features need to be added.
- The commons module for this project encapsulates things like resources, drawables, base activities/fragments, generic view-objects, global dependencies like Architecture components and retrofit, etc.
- The app module corresponds to the app-specific logic, like fragments, DB, viewmodels, view-objects, etc.


package com.ingandresath.zemogaposts.app.vo;

import android.arch.persistence.room.Embedded;

/**
 * @author Andrés Atehortúa
 */
public class PostDetails {

    @Embedded
    private Post post;
    @Embedded
    private User user;

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

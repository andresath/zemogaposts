package com.ingandresath.zemogaposts.app.ui;

import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ingandresath.zemogaposts.app.AllPostListFragment;
import com.ingandresath.zemogaposts.app.FavoritePostListFragment;
import com.ingandresath.zemogaposts.app.R;

/**
 * @author Andrés Atehortúa
 */
public class PostsPagerAdapter extends FragmentStatePagerAdapter {

    private Resources resources;

    public PostsPagerAdapter(FragmentManager fm, Resources resources) {
        super(fm);
        this.resources = resources;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new AllPostListFragment();
        } else {
            return new FavoritePostListFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return resources.getString(R.string.posts_tab_all);
        } else {
            return resources.getString(R.string.posts_tab_favorites);
        }
    }
}

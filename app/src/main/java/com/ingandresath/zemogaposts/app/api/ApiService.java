package com.ingandresath.zemogaposts.app.api;

import com.ingandresath.zemogaposts.app.vo.Post;
import com.ingandresath.zemogaposts.app.vo.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * @author Andrés Atehortúa
 */
public interface ApiService {

    @GET("/posts")
    Call<List<Post>> getPosts();

    @GET("/users")
    Call<List<User>> getUsers();

}

package com.ingandresath.zemogaposts.app.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import com.ingandresath.zemogaposts.app.vo.Post;
import com.ingandresath.zemogaposts.app.vo.PostDetails;
import com.ingandresath.zemogaposts.app.vo.User;

import java.util.List;

/**
 * @author Andrés Atehortúa
 */
@Dao
public interface ContentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPosts(List<Post> posts);

    @Query("SELECT * FROM Post")
    LiveData<List<Post>> getAllPosts();

    @Query("SELECT * FROM Post"
            + " WHERE Post.post_isFavorite = 1")
    LiveData<List<Post>> getFavoritePosts();

    @Query("SELECT * FROM Post"
            + " WHERE Post.post_userId = :userId"
            + " AND Post.post_id != :postId")
    LiveData<List<Post>> getRelatedPosts(@NonNull String userId, @NonNull String postId);

    @Delete
    void removePost(@NonNull Post post);

    @Query("DELETE FROM Post")
    void removeAllPosts();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUsers(List<User> users);

    @Query("SELECT * FROM User")
    LiveData<List<User>> getAllUsers();

    @Query("SELECT * FROM Post"
            + " JOIN User ON Post.post_userId = User.user_id"
            + " WHERE Post.post_id = :postId")
    LiveData<PostDetails> getPostDetails(@NonNull String postId);

    @Query("UPDATE Post SET post_isFavorite = :favorite"
            + " WHERE Post.post_id = :postId")
    void favoritePost(@NonNull String postId, boolean favorite);

    @Query("UPDATE Post SET post_isUnread = :unread"
            + " WHERE Post.post_id = :postId")
    void markPostAsUnread(@NonNull String postId, boolean unread);
}

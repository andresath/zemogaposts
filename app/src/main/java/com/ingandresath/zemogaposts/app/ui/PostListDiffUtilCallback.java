package com.ingandresath.zemogaposts.app.ui;

import android.support.v7.util.DiffUtil;

import com.ingandresath.zemogaposts.app.vo.Post;

import java.util.List;

/**
 * @author Andrés Atehortúa
 */
public class PostListDiffUtilCallback extends DiffUtil.Callback {

    private List<Post> oldPostList;
    private List<Post> newPostList;

    public PostListDiffUtilCallback(List<Post> oldPostList, List<Post> newPostList) {
        this.oldPostList = oldPostList;
        this.newPostList = newPostList;
    }

    @Override
    public int getOldListSize() {
        return oldPostList.size();
    }

    @Override
    public int getNewListSize() {
        return newPostList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldPostList.get(oldItemPosition).getId().equals(newPostList.get(newItemPosition).getId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldPostList.get(oldItemPosition).equals(newPostList.get(newItemPosition));
    }
}

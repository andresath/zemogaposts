package com.ingandresath.zemogaposts.app.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import com.ingandresath.zemogaposts.app.respository.ContentRepository;
import com.ingandresath.zemogaposts.vo.Resource;

/**
 * @author Andrés Atehortúa
 */
public class ContentViewModel extends ViewModel {

    private ContentRepository repository;
    private LiveData<Resource<Boolean>> repoResultLiveData;
    private MediatorLiveData<Resource<Boolean>> contentRefreshResourceLiveData;

    public ContentViewModel() {
        repository = ContentRepository.getInstance();
        contentRefreshResourceLiveData = new MediatorLiveData<>();
    }

    public LiveData<Resource<Boolean>> getRefreshProcess() {
        return contentRefreshResourceLiveData;
    }

    public void refresh() {
        contentRefreshResourceLiveData.removeSource(repoResultLiveData);
        repoResultLiveData = repository.refreshContent();
        contentRefreshResourceLiveData.addSource(repoResultLiveData, new Observer<Resource<Boolean>>() {
            @Override
            public void onChanged(@Nullable Resource<Boolean> booleanResource) {
                contentRefreshResourceLiveData.setValue(booleanResource);
            }
        });
    }

    public void removeAllPosts() {
        repository.removeAllPosts();
    }
}

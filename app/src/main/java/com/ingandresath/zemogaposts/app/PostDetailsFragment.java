package com.ingandresath.zemogaposts.app;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ingandresath.zemogaposts.app.ui.PostListAdapter;
import com.ingandresath.zemogaposts.app.viewmodel.PostDetailsViewModel;
import com.ingandresath.zemogaposts.app.vo.Post;
import com.ingandresath.zemogaposts.app.vo.PostDetails;
import com.ingandresath.zemogaposts.ui.BaseFragment;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostDetailsFragment extends BaseFragment {

    private static final String ARGS_POST_ID = "postId";

    private PostDetailsViewModel viewModel;

    private CheckBox favoriteCheckBox;
    private TextView postBodyTextView;
    private TextView userInfoTextView;
    private PostListAdapter adapter;

    public static PostDetailsFragment newInstance(@NonNull String postId) {
        Bundle args = new Bundle();
        args.putString(ARGS_POST_ID, postId);

        PostDetailsFragment fragment = new PostDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders.of(this).get(PostDetailsViewModel.class);
        viewModel.setPostId(getArguments().getString(ARGS_POST_ID));
        adapter = new PostListAdapter(null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_post_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");

        postBodyTextView = view.findViewById(R.id.postDetails_description);
        userInfoTextView = view.findViewById(R.id.postDetails_user_info);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        viewModel.getPostDetailsLiveData().observe(this, new Observer<PostDetails>() {
            @Override
            public void onChanged(@Nullable PostDetails postDetails) {
                favoriteCheckBox.setChecked(postDetails.getPost().isFavorite());
                postBodyTextView.setText(postDetails.getPost().getBody());
                userInfoTextView.setText(getString(R.string.postDetails_user_info_format,
                        postDetails.getUser().getName(),
                        postDetails.getUser().getEmail(),
                        postDetails.getUser().getPhone(),
                        postDetails.getUser().getWebsite()));
            }
        });

        viewModel.getRelatedPostsLiveData().observe(this, new Observer<List<Post>>() {
            @Override
            public void onChanged(@Nullable List<Post> posts) {
                adapter.setPosts(posts);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        viewModel.getPostDetailsLiveData().removeObservers(this);
        viewModel.getRelatedPostsLiveData().removeObservers(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.post_details_menu, menu);

        final MenuItem favoriteMenuItem = menu.findItem(R.id.action_favorite);

        final FrameLayout favoriteRootView = (FrameLayout) favoriteMenuItem.getActionView();
        favoriteCheckBox = favoriteRootView.findViewById(R.id.favoriteCheckBox);
        favoriteCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                viewModel.favoritePost(isChecked);
            }
        });
    }
}

package com.ingandresath.zemogaposts.app.ui;

import com.ingandresath.zemogaposts.app.vo.Post;

/**
 * @author Andrés Atehortúa
 */
public interface OnPostSelectListener {

    void onPostSelected(Post post);
}

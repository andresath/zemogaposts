package com.ingandresath.zemogaposts.app;

import android.app.Application;

import com.ingandresath.zemogaposts.app.api.ApiClient;
import com.ingandresath.zemogaposts.app.db.ContentDatabase;

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        initDependencies();
    }

    private void initDependencies() {
        ApiClient.init(this);
        ContentDatabase.init(this);
    }
}

package com.ingandresath.zemogaposts.app.vo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * @author Andrés Atehortúa
 */
@Entity
public class User {

    @SerializedName("id")
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "user_id")
    private String id = "-1";
    @SerializedName("name")
    @ColumnInfo(name = "user_name")
    private String name;
    @SerializedName("email")
    @ColumnInfo(name = "user_email")
    private String email;
    @SerializedName("phone")
    @ColumnInfo(name = "user_phone")
    private String phone;
    @SerializedName("website")
    @ColumnInfo(name = "user_website")
    private String website;

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}

package com.ingandresath.zemogaposts.app.api;

import android.app.Application;
import android.content.Context;

import com.ingandresath.zemogaposts.app.vo.Post;
import com.ingandresath.zemogaposts.app.vo.User;

import java.io.File;
import java.util.List;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Andrés Atehortúa
 */
public class ApiClient {

    private static final String CACHE_DIR = "http-cache";
    private static final int CACHE_SIZE = 10 * 1024 * 1024; //10MB
    private static final String BASE_URL = "https://jsonplaceholder.typicode.com";

    private static ApiClient instance;

    private final ApiService service;

    private ApiClient(Context context) {
        final Cache cache = new Cache(new File(context.getCacheDir(), CACHE_DIR), CACHE_SIZE);
        final OkHttpClient clientBuilder = new OkHttpClient.Builder()
                .cache(cache)
                .build();
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientBuilder)
                .build();

        service = retrofit.create(ApiService.class);
    }

    public static void init(Application application) {
        instance = new ApiClient(application);
    }

    public static ApiClient getInstance() {
        if (instance == null) {
            throw new NullPointerException("Did you forget to call init?");
        }
        return instance;
    }

    public void getPosts(Callback<List<Post>> callback) {
        service.getPosts().enqueue(callback);
    }

    public void getUsers(Callback<List<User>> callback) {
        service.getUsers().enqueue(callback);
    }
}

package com.ingandresath.zemogaposts.app;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ingandresath.zemogaposts.app.ui.PostsPagerAdapter;
import com.ingandresath.zemogaposts.app.viewmodel.ContentViewModel;
import com.ingandresath.zemogaposts.ui.BaseFragment;
import com.ingandresath.zemogaposts.vo.Resource;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostsFragment extends BaseFragment {

    private ContentViewModel contentViewModel;
    private PostsPagerAdapter pagerAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        contentViewModel = ViewModelProviders.of(this).get(ContentViewModel.class);
        pagerAdapter = new PostsPagerAdapter(getChildFragmentManager(), getResources());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_posts, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.posts_title);

        ViewPager viewPager = view.findViewById(R.id.viewPager);
        viewPager.setAdapter(pagerAdapter);
        TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);

        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contentViewModel.removeAllPosts();
            }
        });

        contentViewModel.getRefreshProcess().observe(this, new Observer<Resource<Boolean>>() {
            @Override
            public void onChanged(@Nullable Resource<Boolean> booleanResource) {
                if (booleanResource == null) {
                    return;
                }

                switch (booleanResource.status) {
                    case LOADING:
                        showProgressDialog();
                        break;
                    case SUCCESS:
                        hideProgressDialog();
                        break;
                    case ERROR:
                        hideProgressDialog();
                        showServiceErrorBanner();
                        break;
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        contentViewModel.getRefreshProcess().removeObservers(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.post_list_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh_posts:
                contentViewModel.refresh();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

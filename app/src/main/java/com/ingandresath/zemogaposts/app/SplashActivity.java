package com.ingandresath.zemogaposts.app;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ingandresath.zemogaposts.ui.BaseActivity;

/**
 * @author Andrés Atehortúa
 */
public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addFragment(new SplashFragment());
    }
}

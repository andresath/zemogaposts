package com.ingandresath.zemogaposts.app.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingandresath.zemogaposts.app.R;
import com.ingandresath.zemogaposts.app.vo.Post;

/**
 * @author Andrés Atehortúa
 */
public class PostListItemViewHolder extends RecyclerView.ViewHolder {

    private TextView titleTextView;
    private ImageView unreadIndicatorImageView;
    private ImageView favoriteIndicatorImageView;

    public PostListItemViewHolder(ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_post_list_item, parent, false));

        titleTextView = itemView.findViewById(R.id.titleTextView);
        unreadIndicatorImageView = itemView.findViewById(R.id.unreadIndicatorImageView);
        favoriteIndicatorImageView = itemView.findViewById(R.id.favoriteIndicatorImageView);
    }

    public void bind(final Post post, final OnPostSelectListener listener) {
        titleTextView.setText(post.getTitle());
        unreadIndicatorImageView.setVisibility(post.isUnread() ? View.VISIBLE : View.GONE);
        favoriteIndicatorImageView.setVisibility(post.isFavorite() ? View.VISIBLE : View.GONE);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onPostSelected(post);
                }
            }
        });
    }
}

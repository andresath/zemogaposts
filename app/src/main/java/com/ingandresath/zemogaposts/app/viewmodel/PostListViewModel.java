package com.ingandresath.zemogaposts.app.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.ingandresath.zemogaposts.app.respository.ContentRepository;
import com.ingandresath.zemogaposts.app.vo.Post;

import java.util.List;

/**
 * @author Andrés Atehortúa
 */
public class PostListViewModel extends ViewModel {

    private ContentRepository repository;
    private LiveData<List<Post>> allPostsLiveData;
    private LiveData<List<Post>> favoritePostsLiveData;

    public PostListViewModel() {
        repository = ContentRepository.getInstance();

        allPostsLiveData = repository.getAllPosts();
        favoritePostsLiveData = repository.getFavoritePosts();
    }

    public LiveData<List<Post>> getAllPostsLiveData() {
        return allPostsLiveData;
    }

    public LiveData<List<Post>> getFavoritePostsLiveData() {
        return favoritePostsLiveData;
    }

    public void removePost(Post post) {
        repository.removePost(post);
    }

    public void markPostAsUnread(String postId, boolean unread) {
        repository.markPostAsUnread(postId, unread);
    }
}

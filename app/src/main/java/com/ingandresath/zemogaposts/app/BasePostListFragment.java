package com.ingandresath.zemogaposts.app;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingandresath.zemogaposts.app.ui.OnPostSelectListener;
import com.ingandresath.zemogaposts.app.ui.PostListAdapter;
import com.ingandresath.zemogaposts.app.viewmodel.PostListViewModel;
import com.ingandresath.zemogaposts.app.vo.Post;
import com.ingandresath.zemogaposts.ui.BaseFragment;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BasePostListFragment extends BaseFragment {

    //region Listener handling logic.

    private OnPostSelectListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPostSelectListener) {
            listener = (OnPostSelectListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnPostSelectListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    //endregion

    private PostListViewModel viewModel;
    private PostListAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders.of(getActivity()).get(PostListViewModel.class);
        adapter = new PostListAdapter(new OnPostSelectListener() {
            @Override
            public void onPostSelected(Post post) {
                viewModel.markPostAsUnread(post.getId(), false);
                listener.onPostSelected(post);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_post_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                final Post post = adapter.getPost(viewHolder.getAdapterPosition());
                viewModel.removePost(post);
            }
        });
        itemTouchHelper.attachToRecyclerView(recyclerView);

        getPostsLiveData(viewModel).observe(this, new Observer<List<Post>>() {
            @Override
            public void onChanged(@Nullable List<Post> posts) {
                adapter.setPosts(posts);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        getPostsLiveData(viewModel).removeObservers(this);
    }

    protected abstract LiveData<List<Post>> getPostsLiveData(PostListViewModel viewModel);
}

package com.ingandresath.zemogaposts.app.ui;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.ingandresath.zemogaposts.app.vo.Post;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrés Atehortúa
 */
public class PostListAdapter extends RecyclerView.Adapter<PostListItemViewHolder> {

    private List<Post> postList;
    private OnPostSelectListener listener;

    public PostListAdapter(OnPostSelectListener listener) {
        postList = new ArrayList<>();
        this.listener = listener;
    }

    public void setPosts(List<Post> postList) {
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(new PostListDiffUtilCallback(this.postList, postList));
        diffResult.dispatchUpdatesTo(this);

        this.postList = postList;
    }

    public Post getPost(int position) {
        return postList.get(position);
    }

    @NonNull
    @Override
    public PostListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PostListItemViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull PostListItemViewHolder holder, int position) {
        holder.bind(postList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }
}

package com.ingandresath.zemogaposts.app;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingandresath.zemogaposts.app.viewmodel.ContentViewModel;
import com.ingandresath.zemogaposts.ui.BaseFragment;
import com.ingandresath.zemogaposts.vo.Resource;

/**
 * A simple {@link Fragment} subclass.
 */
public class SplashFragment extends BaseFragment {

    private static final long DELAY_MS = 1000;

    private ContentViewModel viewModel;

    public SplashFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders.of(this).get(ContentViewModel.class);
        viewModel.refresh();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_splash, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel.getRefreshProcess().observe(this, new Observer<Resource<Boolean>>() {
            @Override
            public void onChanged(@Nullable Resource<Boolean> booleanResource) {
                if (booleanResource == null) {
                    return;
                }

                switch (booleanResource.status) {
                    case SUCCESS:
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                goToMainScreen();
                            }
                        }, DELAY_MS);
                        break;
                    case ERROR:
                        showServiceErrorBanner();
                        break;
                }
            }
        });
    }

    private void goToMainScreen() {
        Intent intent = new Intent(getContext(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        viewModel.getRefreshProcess().removeObservers(this);
    }
}

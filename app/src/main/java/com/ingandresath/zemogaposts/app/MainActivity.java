package com.ingandresath.zemogaposts.app;

import android.os.Bundle;
import android.view.MenuItem;

import com.ingandresath.zemogaposts.app.ui.OnPostSelectListener;
import com.ingandresath.zemogaposts.app.vo.Post;
import com.ingandresath.zemogaposts.ui.BaseActivity;

public class MainActivity extends BaseActivity implements OnPostSelectListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addFragment(new PostsFragment());
    }

    @Override
    public void onPostSelected(Post post) {
        replaceFragment(PostDetailsFragment.newInstance(post.getId()));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                upNavigate();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

package com.ingandresath.zemogaposts.app.respository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;

import com.ingandresath.zemogaposts.app.api.ApiClient;
import com.ingandresath.zemogaposts.app.db.ContentDatabase;
import com.ingandresath.zemogaposts.app.vo.Post;
import com.ingandresath.zemogaposts.app.vo.PostDetails;
import com.ingandresath.zemogaposts.app.vo.User;
import com.ingandresath.zemogaposts.vo.Resource;
import com.ingandresath.zemogaposts.vo.Status;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author Andrés Atehortúa
 */
public class ContentRepository {

    private static ContentRepository instance;

    private final ApiClient apiClient;

    private ContentRepository() {
        apiClient = ApiClient.getInstance();
    }

    public static ContentRepository getInstance() {
        if (instance == null) {
            instance = new ContentRepository();
        }
        return instance;
    }

    public LiveData<Resource<Boolean>> refreshContent() {
        final MediatorLiveData<Resource<Boolean>> result = new MediatorLiveData<>();

        result.setValue(Resource.loading(true));

        final LiveData<Resource<List<Post>>> remotePostsSource = getPostsFromRemote();
        result.addSource(remotePostsSource, new Observer<Resource<List<Post>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<Post>> listResource) {
                result.removeSource(remotePostsSource);

                if (listResource.status == Status.SUCCESS) {
                    storePosts(listResource.data);

                    final LiveData<Resource<List<User>>> remoteUsersSource = getUsersFromRemote();
                    result.addSource(remoteUsersSource, new Observer<Resource<List<User>>>() {
                        @Override
                        public void onChanged(@Nullable Resource<List<User>> listResource) {
                            result.removeSource(remoteUsersSource);

                            if (listResource.status == Status.SUCCESS) {
                                storeUsers(listResource.data);
                                result.setValue(Resource.success(true));
                            } else {
                                result.setValue(Resource.error(listResource.message, true));
                            }
                        }
                    });
                } else {
                    result.setValue(Resource.error(listResource.message, true));
                }
            }
        });

        return result;
    }

    private LiveData<Resource<List<Post>>> getPostsFromRemote() {
        final MutableLiveData<Resource<List<Post>>> result = new MutableLiveData<>();

        apiClient.getPosts(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if (response.isSuccessful()) {
                    List<Post> posts = response.body();
                    result.setValue(Resource.success(posts));
                } else {
                    result.setValue(Resource.<List<Post>>error(
                            String.format("Error Code: %s", response.code()), null));
                }
            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                result.setValue(Resource.<List<Post>>error(
                        "Unknown Failure", null));
            }
        });

        return result;
    }

    private void storePosts(final List<Post> posts) {
        final Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < posts.size(); i++) {
                    Post post = posts.get(i);
                    if (i < 20) {
                        post.setUnread(true);
                    } else {
                        post.setUnread(false);
                    }
                }
                ContentDatabase.getInstance().contentDao().insertPosts(posts);
            }
        });
    }

    private LiveData<Resource<List<User>>> getUsersFromRemote() {
        final MutableLiveData<Resource<List<User>>> result = new MutableLiveData<>();

        apiClient.getUsers(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.isSuccessful()) {
                    List<User> posts = response.body();
                    result.setValue(Resource.success(posts));
                } else {
                    result.setValue(Resource.<List<User>>error(
                            String.format("Error Code: %s", response.code()), null));
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                result.setValue(Resource.<List<User>>error(
                        "Unknown Failure", null));
            }
        });

        return result;
    }

    private void storeUsers(final List<User> users) {
        final Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                ContentDatabase.getInstance().contentDao().insertUsers(users);
            }
        });
    }

    public LiveData<List<Post>> getAllPosts() {
        return ContentDatabase.getInstance().contentDao().getAllPosts();
    }

    public LiveData<List<Post>> getFavoritePosts() {
        return ContentDatabase.getInstance().contentDao().getFavoritePosts();
    }

    public LiveData<List<Post>> getRelatedPosts(String userId, String postId) {
        return ContentDatabase.getInstance().contentDao().getRelatedPosts(userId, postId);
    }

    public void removePost(final Post post) {
        final Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                ContentDatabase.getInstance().contentDao().removePost(post);
            }
        });
    }

    public void removeAllPosts() {
        final Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                ContentDatabase.getInstance().contentDao().removeAllPosts();
            }
        });
    }

    public LiveData<PostDetails> getPostDetails(String postId) {
        return ContentDatabase.getInstance().contentDao().getPostDetails(postId);
    }

    public void favoritePost(final String postId, final boolean favorite) {
        final Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                ContentDatabase.getInstance().contentDao().favoritePost(postId, favorite);
            }
        });
    }

    public void markPostAsUnread(final String postId, final boolean unread) {
        final Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                ContentDatabase.getInstance().contentDao().markPostAsUnread(postId, unread);
            }
        });
    }
}

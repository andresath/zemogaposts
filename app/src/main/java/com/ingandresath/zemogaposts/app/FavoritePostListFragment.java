package com.ingandresath.zemogaposts.app;

import android.arch.lifecycle.LiveData;
import android.support.v4.app.Fragment;

import com.ingandresath.zemogaposts.app.viewmodel.PostListViewModel;
import com.ingandresath.zemogaposts.app.vo.Post;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritePostListFragment extends BasePostListFragment {

    public FavoritePostListFragment() {
        // Required empty public constructor
    }

    @Override
    protected LiveData<List<Post>> getPostsLiveData(PostListViewModel viewModel) {
        return viewModel.getFavoritePostsLiveData();
    }
}

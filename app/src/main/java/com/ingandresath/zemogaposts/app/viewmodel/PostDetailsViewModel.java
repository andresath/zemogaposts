package com.ingandresath.zemogaposts.app.viewmodel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.ingandresath.zemogaposts.app.respository.ContentRepository;
import com.ingandresath.zemogaposts.app.vo.Post;
import com.ingandresath.zemogaposts.app.vo.PostDetails;

import java.util.List;

/**
 * @author Andrés Atehortúa
 */
public class PostDetailsViewModel extends ViewModel {

    private ContentRepository repository;
    private MutableLiveData<String> postIdInput;
    private LiveData<PostDetails> postDetailsLiveData;
    private LiveData<List<Post>> relatedPostsLiveData;

    public PostDetailsViewModel() {
        repository = ContentRepository.getInstance();

        postIdInput = new MutableLiveData<>();
        postDetailsLiveData = Transformations.switchMap(postIdInput, new Function<String, LiveData<PostDetails>>() {
            @Override
            public LiveData<PostDetails> apply(String postId) {
                return repository.getPostDetails(postId);
            }
        });

        relatedPostsLiveData = Transformations.switchMap(postDetailsLiveData, new Function<PostDetails, LiveData<List<Post>>>() {
            @Override
            public LiveData<List<Post>> apply(PostDetails input) {
                return repository.getRelatedPosts(input.getUser().getId(), input.getPost().getId());
            }
        });
    }

    public void setPostId(String postId) {
        postIdInput.setValue(postId);
    }

    public LiveData<PostDetails> getPostDetailsLiveData() {
        return postDetailsLiveData;
    }

    public LiveData<List<Post>> getRelatedPostsLiveData() {
        return relatedPostsLiveData;
    }

    public void favoritePost(boolean favorite) {
        repository.favoritePost(postIdInput.getValue(), favorite);
    }
}

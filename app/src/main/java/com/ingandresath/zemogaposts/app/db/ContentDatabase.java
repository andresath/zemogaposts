package com.ingandresath.zemogaposts.app.db;

import android.app.Application;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

import com.ingandresath.zemogaposts.app.vo.Post;
import com.ingandresath.zemogaposts.app.vo.User;

@Database(
        entities = {
                Post.class,
                User.class
        }, version = 1, exportSchema = false)
public abstract class ContentDatabase extends RoomDatabase {

    private static final String DB_NAME = "content_db";

    private static ContentDatabase instance;

    public static void init(Application application) {
        instance = Room.databaseBuilder(application, ContentDatabase.class, DB_NAME).build();
    }

    public static ContentDatabase getInstance() {
        if (instance == null) {
            throw new NullPointerException("Did you forget to call init?");
        }
        return instance;
    }

    public abstract ContentDao contentDao();
}

package com.ingandresath.zemogaposts.app.vo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * @author Andrés Atehortúa
 */
@Entity
public class Post {

    @SerializedName("id")
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "post_id")
    private String id = "-1";
    @SerializedName("userId")
    @ColumnInfo(name = "post_userId")
    private String userId;
    @SerializedName("title")
    @ColumnInfo(name = "post_title")
    private String title;
    @SerializedName("body")
    @ColumnInfo(name = "post_body")
    private String body;
    @ColumnInfo(name = "post_isUnread")
    private boolean unread;
    @ColumnInfo(name = "post_isFavorite")
    private boolean favorite;

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isUnread() {
        return unread;
    }

    public void setUnread(boolean unread) {
        this.unread = unread;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
}
